# Prospects

###O teste:
O projeto deve conter 2 menus:

1. Listagem de prospects, composta por uma tabela
	- Dados a mostrar: data de inserção, setor, nome, estado atual, data do próxima ação, progress bar do estado do prospect* e link para editar o prospect
	- Botão para inserir prospect

2. Página para editar/inserir prospect
	- Form com os seguintes campos:
		- Nome do prospect, sector e contactos
		- Ação.

A ação é composta pelas seguintes fases, 1º contato, marcar reunião, reunião, adjudicou, não adjudicou
(*) a progress bar é:
	1º contato -> 0%
	marcar reunião -> 33%
	reunião -> 66%
	adjudicou-> 100% verde
	não adjudicou-> 100% vermelha

As ações devem ser guardadas em MySQL, assim como o prospect, os setores e o histórico das ações.
Deve utilizar no minimo 1 stored procedure.
Deve utilizar um trigger a cada novo prospect, que deve replicar o mesmo para uma tabela chamada prospect_copy.
Este projeto deve ser desenvolvido em HTML, CSS, PHP, JQuery, MySQL e utilizar a framework CodeIgniter.

Todo o layout que for usado é opcional e fica à consideração do candidato.

---
# Configurando o Projeto
#####Download
No seu diretorio de projetos web faça o clone do projeto:
```bash
$ cd /var/www/html
$ git clone https://w-osilva@bitbucket.org/w-osilva/prospects.git
```
#####Apache
1 - Copie o arquivo prospect.conf que está na raiz do diretório do projeto para o aparche.
```bash
$ cd /var/www/html/prospects
$ sudo cp prospect.conf /etc/apache2/sites-available/
```

2 - Habilite a conf no apache e recarrege o serviço:
```bash
$ sudo a2ensite prospect.conf
$ sudo service apache2 reload
```

3 - Configure seu arquivo **/etc/hosts** para resolver o host do projeto:
```bash
$ sudo gedit /etc/hosts
```
Adicione e depois salve o arquivo:
```bash
127.0.0.1  dev.prospect
```

#####Mysql
Para criar as tabelas, execute os seguintes comandos substituindo os dados de usuario pelos seus:
```bash
$ cd /var/www/html/prospects
$ mysql -u root -p root -l 127.0.0.1 prospect < sql/migration.sql
```

**OBS:** Não esquecer de configurar os dados do Banco no arquivo de configuração de databases...

Agora tente acesar o projeto http://dev.prospect/

