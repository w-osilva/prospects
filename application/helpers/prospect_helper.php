
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');




const PERCENTUAL_PASSO_1 = 0;
const PERCENTUAL_PASSO_2 = 33;
const PERCENTUAL_PASSO_3 = 66;
const PERCENTUAL_PASSO_4 = 100;
const PERCENTUAL_PASSO_5 = 100;

const PASSO_1 = 1;
const PASSO_2 = 2;
const PASSO_3 = 3;
const PASSO_4 = 4;
const PASSO_5 = 5;

function getPercentToCodeStatus($status){

	if (PASSO_1 == $status){
		return PERCENTUAL_PASSO_1;
	} elseif (PASSO_2 == $status){
		return PERCENTUAL_PASSO_2;
	} elseif (PASSO_3 == $status){
		return PERCENTUAL_PASSO_3;
	} elseif (PASSO_4 == $status){
		return PERCENTUAL_PASSO_4;
	} elseif (PASSO_5 == $status){
		return PERCENTUAL_PASSO_5;
	}

	return false;
}

