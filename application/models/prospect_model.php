<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prospect_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	
	function findAll(){
		// ->result(); devolve todas as linhas
		// ->row(); devolve uma unica linha
        return $this->db->get('prospect')->result(); // SELECT *
    }

    function findById(){
        $this->db->select('prospect.*');
        $this->db->from('prospect');
        $this->db->join('marcas', 'produtos.cod_marca = marcas.cod_marca'. 'left');

        return $this->db->get('prospect')->row(); // SELECT *
    }

	function insertProspect($dados){
        return $this->db->insert('prospect', $dados);
	}

	function insertProspectStep($dados){
		return $this->db->insert('prospects_steps', $dados);
	}

    function editar($prospect, $dados){
		$this->db->where('prospect', $prospect);
		return $this->db->update('prospect', $dados);		
	}
	
	
}