<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prospect extends CI_Controller {

	const PASSO_1 = 1;
	const PASSO_2 = 2;
	const PASSO_3 = 3;
	const PASSO_4 = 4;
	const PASSO_5 = 5;

	public function __construct(){
		parent::__construct();
		$this->load->model('prospect_model');
		//$this->load->helper('prospect');
	}

	
	public function index(){
		
		$lista = $this->prospect_model->findAll();

		$this->load->view('template_header');
		$this->load->view('prospect/index', array(
			'prospects'=> $lista,
			//'prospect_helper' => $this->prospect
		));
		$this->load->view('template_footer');

	}	
		
	public function cadastrar(){
		
		//define as regras de validação do formulário		
		$this->form_validation->set_rules('nome', 'Nome', 'xss_clean|required');
		$this->form_validation->set_rules('data_prox_acao', 'Data da proxima acao', 'xss_clean|required');
		$this->form_validation->set_rules('setor', 'Setor', 'xss_clean|required' );
		$this->form_validation->set_rules('acao', 'Acao', 'xss_clean|required');

		//verifica se o formulário passou na validação
		if($this->form_validation->run()){
			$prospect = array(
				'nome' => $this->input->post('nome'),
				'data_prox_acao' => $this->input->post('data_prox_acao'),
				'data_insercao' => date("Y-m-d"),
				'estado_atual' => $this->input->post('acao')//Passa o codigo da acao para definir o status (ver constantes da classe)
				
			);

			$prospectId = $this->prospect_model->insertProspect($prospect);
			if ($prospectId) {
				$step = array(
					'prospect_id' => $prospectId,
					'setor' => $this->input->post('setor'),
					'acao' => $this->input->post('acao')
				);
				$this->prospect_model->insertProspectStep($step);

				//define a mensagem de sucesso
				$this->session->set_flashdata('message', '<p class="alert alert-success"><button class="close" data-dismiss="alert">×</button> Cadastrado efetuado com sucesso</p>');
				
				//redireciona para a lista 
				redirect('prospect', 'refresh');
			}
		}
		$this->load->view('template_header');
		$this->load->view('prospect/cadastrar');
		$this->load->view('template_footer');
		
	}


    public function editar($id){

        //define as regras de validação do formulário
        $this->form_validation->set_rules('nome', 'Nome', 'xss_clean|required');
        $this->form_validation->set_rules('data_prox_acao', 'Data da proxima acao', 'xss_clean|required');
        $this->form_validation->set_rules('setor', 'Setor', 'xss_clean|required' );
        $this->form_validation->set_rules('acao', 'Acao', 'xss_clean|required');

        //verifica se o formulário passou na validação
        if($this->form_validation->run()){

            $prospect = array(
                'nome' => $this->input->post('nome'),
                'data_prox_acao' => $this->input->post('data_prox_acao'),
                'data_insercao' => date("Y-m-d"),
                'estado_atual' => $this->input->post('acao')//Passa o codigo da acao para definir o status (ver constantes da classe)
            );

            if($this->prospect_model->updateProspect($id, $prospect)) {
                //define a mensagem de sucesso
                $step = array(
                    'prospect_id' => $id,
                    'setor' => $this->input->post('setor'),
                    'acao' => $this->input->post('acao')
                );

                if($this->prospect_model->updateProspectStep($id, $step)){
                    $this->session->set_flashdata('message', '<p class="alert alert-success"><button class="close" data-dismiss="alert">×</button> Atualização efetuada com sucesso</p>');
                    redirect('prospect', 'refresh');
                }

            }

        }

        $prospect = $this->prospect_model->findById($id);

        $this->load->view('template_header');
        $this->load->view('prospect/editar', array('prospect'=> $prospect));
        $this->load->view('template_footer');

    }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */