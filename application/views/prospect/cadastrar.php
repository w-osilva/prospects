<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cadastrar</title>
</head>
<body>

<div class="container">
    <h3>Cadastro Prospect</h3>

	<form class="form-group" action="<?php echo base_url('prospect/cadastrar')?>" method="post">
        <?php echo validation_errors(); ?>


        <label for="nome" class="control-label">Nome</label>
        <input class="form-control" type="text" name="nome" id="nome" value='<?php echo set_value('nome') ?>'>


        <label for="data_prox_acao" class="control-label">Data da Próxima Ação</label>
        <input class="form-control" type="date" name="data_prox_acao" id="data_prox_acao"  value="<?php echo set_value('data_prox_acao'); ?>">


        <label for="setor" class="control-label">Setor</label>
        <input class="form-control" type="text" name="setor" id="setor" value="<?php echo set_value('setor'); ?>">


		<label for="acao" class="ccontrol-label">Ação</label>
        <select class="form-control" name="acao" id="acao">
            <option value="">Selecione</option>
            <option value="1">1º Contato</option>
            <option value="2">Marcar Reunião - 33%</option>
            <option value="3">Reunião - 66%</option>
            <option value="4">Ajudicou - 100% </option>
            <option value="5">Não Ajudicou - 100% </option>
        </select>

        <input class="btn btn-primary" type="submit" value="Cadastrar" id="myButton" data-loading-text="Loading..." autocomplete="off">


    </form>
</div>
</body>
</html>