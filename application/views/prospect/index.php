
<div class="container-fluid">

	<a href="<?= base_url('prospect/cadastrar') ?>" class="btn btn-success">Novo</a>
	
	<table class="table table bordered table-striped">
		<tr>
			<th>Nome</th>
			<th>Estado Atual</th>
			<th>Data Inserção</th>
			<th>Data Proxima Ação</th>			
		</tr>

		<?php foreach ($prospects as  $prospect): ?>
		<tr>
			<td><?= $prospect->nome?></td>
			<td>
			   	<div class="progress">	  					
						<div class="progress-bar 
						<?php if ($prospect->estado_atual == 4 ){ echo 'progress-bar-success'; } elseif ($prospect->estado_atual == 5 ){
						 echo 'progress-bar-danger'; } ?>" role="progressbar" aria-valuenow="<?php echo getPercentToCodeStatus($prospect->estado_atual) ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo getPercentToCodeStatus($prospect->estado_atual) ?>%">
						<?php echo getPercentToCodeStatus($prospect->estado_atual) ?>%
				</div>
			</td>
			<td><?= $prospect->data_insercao?></td>
			<td><?= $prospect->data_prox_acao?></td>
		</tr>

		<?php endforeach;?>
	</table>

</div>
