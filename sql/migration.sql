-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 17-Mar-2015 às 01:37
-- Versão do servidor: 5.5.41-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `prospects`
--

CREATE DATABASE IF NOT EXISTS `prospects`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prospect`
--

CREATE TABLE IF NOT EXISTS `prospect` (
  `prospect_id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `data_prox_acao` date NOT NULL,
  `data_insercao` date NOT NULL,
  `estado_atual` int(11) NOT NULL,
  PRIMARY KEY (`prospect_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `prospect`
--


--
-- Acionadores `prospect`
--
DROP TRIGGER IF EXISTS `prospect_copy`;
DELIMITER //
CREATE TRIGGER `prospect_copy` AFTER INSERT ON `prospect`
 FOR EACH ROW INSERT INTO prospect_copy 
SELECT * 
FROM prospect
ORDER BY prospect_id DESC
LIMIT 1
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prospects_steps`
--

CREATE TABLE IF NOT EXISTS `prospects_steps` (
  `prospect_id` int(11) NOT NULL,
  `setor` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `acao` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  KEY `prospect_id` (`prospect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `prospects_steps`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `prospect_copy`
--

CREATE TABLE IF NOT EXISTS `prospect_copy` (
  `prospect_id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `data_prox_acao` date NOT NULL,
  `data_insercao` date NOT NULL,
  `estado_atual` int(11) NOT NULL,
  PRIMARY KEY (`prospect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `prospect_copy`
--


--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `prospects_steps`
--
ALTER TABLE `prospects_steps`
  ADD CONSTRAINT `prospects_steps_ibfk_1` FOREIGN KEY (`prospect_id`) REFERENCES `prospect` (`prospect_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;